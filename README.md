# Parallel computing: A sokoban game solver with **OpenMP**


## Sokoban

> “Sokoban a puzzle video game genre in which the player pushes crates or boxes around in a warehouse, trying to get them to storage locations.” - Wikipedia

![img](https://upload.wikimedia.org/wikipedia/commons/4/4b/Sokoban_ani.gif)


## Goal

- Implement a solver for Sokoban called [hw1.cc](./hw1.cc)
- Parallelize it with OpenMP


## Input

- o: The player stepping on a regular tile
- O: The player stepping on a target tile
- x: A box on a regular tile
- X: A box on a target tile
- &nbsp; : Nothing on a regular tile
- .: Nothing on a target tile
- #: Wall
- @: A fragile tile
- !: The player stepping on a fragile tile

[samples/01.txt](./samples/01.txt)
```
#########
#  xox..#
#   #####
#########
```


## Output 

- Print a valid sequence of actions that pushes all the boxes to the target tiles
- Directions: "W", "A", "S", "D"
- e.g.: `DDAAASAAWDDDD` for samples/01.txt


## Usage

1. Compile and execute the source code
    ```
    $ make
    $ ./hw1 samples/01.txt
    ```

**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**
