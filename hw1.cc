#include <cstdio>
#include <iostream>
#include <omp.h>
#include <thread>
#include <cstring>
#include <boost/functional/hash.hpp>
#include <unordered_map>
#include <map>
#include <vector>
#include <array>
#include <queue>


#define DIR_W 0
#define DIR_S 1
#define DIR_A 2
#define DIR_D 3


int mapWidth, mapHeight;
char Map[64][64];
int8_t SpiderTrapMap[64][64];
int8_t newQuotaElementIdx = 0;


// overload ostream << when printing uint8_t
std::ostream & operator<<(std::ostream& o, const uint8_t& num){
    o << (unsigned)num;
    return o;
}
std::ostream & operator<<(std::ostream& o, const int8_t& num){
    o << (int)num;
    return o;
}


// struct definition
struct Coordinate {
    uint8_t X;
    uint8_t Y;

    bool operator<(const Coordinate &rhs) const {
        return (this->Y != rhs.Y) ? (this->Y < rhs.Y) : (this->X < rhs.X);
    }
    bool operator==(const Coordinate &rhs) const {
        return (this->X == rhs.X) && (this->Y == rhs.Y);
    }
    friend std::size_t hash_value(const Coordinate& coord){
        std::size_t seed = 0;
        boost::hash_combine(seed, coord.X);
        boost::hash_combine(seed, coord.Y);
        return seed;
    }
};

std::ostream & operator<<(std::ostream& o, const Coordinate& coord){
    o << "(" << coord.Y << "," << coord.X << ")";
    return o;
}



typedef std::map<Coordinate, std::array<uint8_t, 4>> BoxesState_Type;

std::ostream & operator<<(std::ostream& o, const BoxesState_Type& boxesState){
    for (auto& boxState : boxesState){
        o << boxState.first << "[";
        for (uint8_t dir=0; dir <= 2; dir++) 
            o << boxState.second[dir] << ",";
        o << boxState.second[3] << "] ";
    }
    o << "\n";
    return o;
}



struct MoveTransition {
    Coordinate boxLoc;
    uint8_t dir;
};

std::ostream & operator<<(std::ostream& o, const MoveTransition& moveTrans){
    o << moveTrans.boxLoc;
    switch(moveTrans.dir){
        case DIR_W: o << "^"; break;
        case DIR_S: o << "v"; break;
        case DIR_A: o << "<"; break;
        case DIR_D: o << ">"; break;
    }
    return o;
}



struct State {
    BoxesState_Type boxesState; 
    Coordinate playerLoc;
    State* parentState;
    MoveTransition moveTrans;
    bool isDeadState=0;
    uint8_t numBoxesUnfinished=0; // # of boxes that are not on the target
    uint8_t* coloringMap;
};

std::ostream & operator<<(std::ostream& o, const State& state){
    o << "boxesState: " << &state << "\n" << state.boxesState;
    o << "playerLoc: " << state.playerLoc;
    o << "\nparentState: " << (void*)state.parentState;
    o << "\nmoveTrans:" << state.moveTrans;
    o << "\nisDeadState: " << state.isDeadState;
    o << "\nnumBoxesUnfinished: " << state.numBoxesUnfinished;
    o << "\ncoloringMap:\n";
    for (uint8_t y=0; y <= mapHeight-1; y++){
        for (uint8_t x=0; x <= mapWidth-1; x++)
            o << state.coloringMap[mapWidth*y+x];
        o << '\n';
    }
    return o;
}


struct QuotaElement {
    int8_t boxQuota;
    int8_t chainIdx1;
    int8_t chainIdx2;
};

std::ostream & operator<<(std::ostream& o, const QuotaElement& element){
    o << "(" << element.boxQuota << "[" << element.chainIdx1 << "," << element.chainIdx2 << "]" << ")";
    return o;
}



State* stateRoot = NULL; // root of the state graph
std::unordered_map<BoxesState_Type, State*, boost::hash<BoxesState_Type>> stateHandle; // state handle

auto pq_cmp = [](State* left, State* right) {return left->numBoxesUnfinished > right->numBoxesUnfinished;};
std::priority_queue<State*, std::vector<State*>, decltype(pq_cmp)> pending_queue(pq_cmp); // queue of states to be processed, sorted by numBoxesUnfinished

std::vector<QuotaElement> quotaVec;




// functions declaration
void input_map_and_init_rootState(char* filename);
void collect_movable_dir(BoxesState_Type& boxesState, std::vector<MoveTransition>& movableTransVec);
void bfs_coloring(const BoxesState_Type& boxesState, uint8_t*& coloringMap, Coordinate startPt, uint8_t colorNum);
void construct_coloringMap(const BoxesState_Type& boxesState, uint8_t*& coloringMap);
BoxesState_Type cal_movable_dir(const BoxesState_Type boxesState, Coordinate playerLoc, uint8_t* coloringMap);
bool hasBox(const BoxesState_Type& boxesState, const Coordinate queryCoord);
bool hasBoxOrWall(const BoxesState_Type& boxesState, const Coordinate queryCoord);
bool isTarget(const Coordinate queryCoord);
bool check_if_dead_state(const BoxesState_Type& boxesState, MoveTransition& moveTrans);
std::string dfs_find_move_seq(Coordinate from, Coordinate to, const BoxesState_Type& boxesState, bool* visitMap);
void output_ans(Coordinate currPlayerLoc, State* parentState, MoveTransition lastMoveTrans);
void set_spiderTrapMap(void);




int main(int argc, char** argv){

    // input map & set root state
    stateRoot = new State;
    input_map_and_init_rootState(argv[1]);
    pending_queue.push(stateRoot);


    // set SpiderTrapMap
    set_spiderTrapMap();


    State* processingState;
    bool foundANS = 0;
    while(!foundANS && !pending_queue.empty()){

        // pop a state from the queue
        processingState = pending_queue.top();
        pending_queue.pop();

        // std::cout << *processingState << "\n"; 

        // collect all possiable next move
        std::vector<MoveTransition> movableTransVec;
        collect_movable_dir(processingState->boxesState, movableTransVec);

        
        // test each possiable next move, parallel
        #pragma omp parallel for schedule(dynamic) 
        for (int moveTransIdx=0; moveTransIdx < movableTransVec.size(); moveTransIdx++){
            MoveTransition& movableTrans = movableTransVec[moveTransIdx];

            // update boxesState (new box location)
            Coordinate boxLoc_original = movableTrans.boxLoc;
            Coordinate testStep_boxLoc = movableTrans.boxLoc;
            switch(movableTrans.dir){
                case DIR_W: testStep_boxLoc.Y -= 1; break;
                case DIR_S: testStep_boxLoc.Y += 1; break;
                case DIR_A: testStep_boxLoc.X -= 1; break;
                case DIR_D: testStep_boxLoc.X += 1; break;
            }
            BoxesState_Type testStep_boxesState = processingState->boxesState;
            testStep_boxesState.erase(boxLoc_original);
            testStep_boxesState[testStep_boxLoc] = (std::array<uint8_t,4>){0,0,0,0};
            
            // construct coloringMap & update boxesState (new box movable dir)
            uint8_t* testStep_coloringMap;
            construct_coloringMap(testStep_boxesState, testStep_coloringMap);
            Coordinate testStep_playerLoc = boxLoc_original;
            testStep_boxesState = cal_movable_dir(testStep_boxesState, testStep_playerLoc, testStep_coloringMap);


            // check if this state has existed
            auto testStep_stateIt = stateHandle.find(testStep_boxesState);
            if (testStep_stateIt != stateHandle.end()) { // this state is duplicated
                free(testStep_coloringMap);
            }
            else { // find a new state! 

                // cal some of the remaining components of the state
                State* testStep_parentState = processingState;
                MoveTransition testStep_moveTrans = movableTrans;
                
                uint8_t testStep_numBoxesUnfinished = processingState->numBoxesUnfinished;
                if (Map[boxLoc_original.Y][boxLoc_original.X] == '.') testStep_numBoxesUnfinished++;
                if (Map[testStep_boxLoc.Y][testStep_boxLoc.X] == '.') testStep_numBoxesUnfinished--;

                // check if an ans has been found
                if (testStep_numBoxesUnfinished == 0) { // find an ans
                    free(testStep_coloringMap);

                    #pragma omp critical
                    {
                        output_ans(testStep_playerLoc, testStep_parentState, testStep_moveTrans);
                        putchar('\n');
                        foundANS = 1;
                    }
                } 

                // cal the remaining components of the state
                bool testStep_isDeadState = check_if_dead_state(testStep_boxesState, testStep_moveTrans);
                State* newState;
                newState = new State;
                if (newState == nullptr) exit(1);
                newState->boxesState = testStep_boxesState;
                newState->playerLoc = testStep_playerLoc;
                newState->parentState = testStep_parentState;
                newState->moveTrans = testStep_moveTrans;
                newState->isDeadState = testStep_isDeadState;
                newState->numBoxesUnfinished = testStep_numBoxesUnfinished;
                newState->coloringMap = testStep_coloringMap;

                #pragma omp critical
                {
                    stateHandle[testStep_boxesState] = newState;
                    if (!testStep_isDeadState) pending_queue.push(newState);
                }
            }
        } // end of for each movable
    } // end of while !pending_queue.empty()

    return 0;
}



/* Open the map text file and init Map.
 * filename: filename of thepointer  map */
void input_map_and_init_rootState(char* filename){
    // reset(init) Map to 0 
    std::memset(Map, 0, sizeof(Map));

    // input Map
    std::FILE *mapfile_ptr;
    char tile;
    uint8_t y=0, x=0;
    mapfile_ptr = std::fopen(filename, "r");
    if (mapfile_ptr == NULL){
        std::printf("Cannot open the file.\n");
        exit(1); 
    }

    while(std::fscanf(mapfile_ptr, "%c", &tile) != EOF){
        // note that everything are fixed, except boxes and the player
        Coordinate coord;
        std::array<uint8_t, 4> isMovableDir={0,0,0,0};
        switch(tile){
            case '\n':
                y++; x=0;
                break;
            case 'o':
                (stateRoot->playerLoc) = (Coordinate){.X = x, .Y = y};
                Map[y][x] = ' ';
                x++;
                break;
            case 'O':
                (stateRoot->playerLoc) = (Coordinate){.X = x, .Y = y};
                Map[y][x] = '.';
                x++;
                break;
            case 'x':
                coord = (Coordinate){.X = x, .Y = y};
                stateRoot->boxesState[coord] = isMovableDir;
                stateRoot->numBoxesUnfinished++;
                Map[y][x] = ' ';
                x++;
                break;
            case 'X':
                coord = (Coordinate){.X = x, .Y = y};
                stateRoot->boxesState[coord] = isMovableDir;
                Map[y][x] = '.';
                x++;
                break;
            case ' ':
                Map[y][x] = ' ';
                x++;
                break;
            case '.':
                Map[y][x] = '.';
                x++;
                break;
            case '#':
                Map[y][x] = '#';
                x++;
                break;
            case '@':
                Map[y][x] = '@';
                x++;
                break;
            case '!':
                (stateRoot->playerLoc) = (Coordinate){.X = x, .Y = y};
                Map[y][x] = '@';
                x++;
                break;
            default:
                std::printf("Invalid input: '%c'\n", tile);
                exit(1);
        }
    }

    mapWidth = std::strlen(Map[0]);
    mapHeight = y;

    std::fclose(mapfile_ptr);


    // initialize other components of root
    stateRoot->parentState = NULL;
    stateRoot->isDeadState = 0;
    construct_coloringMap(stateRoot->boxesState, stateRoot->coloringMap);
    stateRoot->boxesState = cal_movable_dir(stateRoot->boxesState, stateRoot->playerLoc, stateRoot->coloringMap);
}

void collect_movable_dir(BoxesState_Type& boxesState, std::vector<MoveTransition>& movableTransVec){
    for (auto& boxState : boxesState){
        Coordinate loc = boxState.first;
        std::array<uint8_t, 4>& movableDir = boxState.second;
        for (uint8_t dir=0; dir < 4; dir++){
            if (movableDir[dir] == 1){
                MoveTransition moveTrans = (MoveTransition){.boxLoc = loc, .dir = dir};
                movableTransVec.push_back(moveTrans);
            } 
        }
    }
}

void bfs_coloring(const BoxesState_Type& boxesState, uint8_t*& coloringMap, Coordinate startPt, uint8_t colorNum){

    std::queue<Coordinate> bfs_queue;
    coloringMap[mapWidth * startPt.Y + startPt.X] = colorNum; 
    bfs_queue.push(startPt);

    while(!bfs_queue.empty()){
        Coordinate pt = bfs_queue.front();
        bfs_queue.pop();

        uint8_t x = pt.X;
        uint8_t y = pt.Y;

        if (y>=2 /*y-1>=1*/ && !hasBoxOrWall(boxesState, (Coordinate){x,(uint8_t)(y-1)}) && coloringMap[mapWidth*(y-1)+x]==0){
            coloringMap[mapWidth*(y-1)+x] = colorNum;
            bfs_queue.push((Coordinate){x,(uint8_t)(y-1)});
        }
        if (y+1<=mapHeight-2 && !hasBoxOrWall(boxesState, (Coordinate){x,(uint8_t)(y+1)}) && coloringMap[mapWidth*(y+1)+x]==0){
            coloringMap[mapWidth*(y+1)+x] = colorNum;
            bfs_queue.push((Coordinate){x,(uint8_t)(y+1)});
        }
        if (x>=2 /*x-1>=1*/ && !hasBoxOrWall(boxesState, (Coordinate){(uint8_t)(x-1),y}) && coloringMap[mapWidth*y+(x-1)]==0){
            coloringMap[mapWidth*y+(x-1)] = colorNum;
            bfs_queue.push((Coordinate){(uint8_t)(x-1),y});
        }
        if (x+1<=mapWidth-2 && !hasBoxOrWall(boxesState, (Coordinate){(uint8_t)(x+1),y}) && coloringMap[mapWidth*y+(x+1)]==0){
            coloringMap[mapWidth*y+(x+1)] = colorNum;
            bfs_queue.push((Coordinate){(uint8_t)(x+1),y});
        }
    }
}

void construct_coloringMap(const BoxesState_Type& boxesState, uint8_t*& coloringMap){
    
    coloringMap = (uint8_t*)std::malloc(mapWidth*mapHeight*sizeof(uint8_t));
    if (coloringMap == NULL) exit(1);
    memset(coloringMap, 0, mapWidth*mapHeight*sizeof(uint8_t));

    uint8_t x=1, y=1; 
    uint8_t colorNum=0;
    
    while (!(y==mapHeight-2 && x==mapWidth-1)){
        if (x == mapWidth-1) {
            y++; x=1;
            continue;
        }
        if (!hasBoxOrWall(boxesState, (Coordinate){x,y}) && coloringMap[mapWidth*y+x]==0){
            colorNum++;
            bfs_coloring(boxesState, coloringMap, (Coordinate){x,y}, colorNum);
        }
        x++;
    }
}

BoxesState_Type cal_movable_dir(const BoxesState_Type boxesState_original, Coordinate playerLoc, uint8_t* coloringMap){
    BoxesState_Type boxesState;
    #pragma omp critical
    {
        boxesState = boxesState_original;
    }
    uint8_t colorNum_player = coloringMap[mapWidth*(playerLoc.Y) + playerLoc.X];

    for (auto it = boxesState.begin(); it != boxesState.end(); it++){
        uint8_t x = it->first.X;
        uint8_t y = it->first.Y;

        it->second = (std::array<uint8_t,4>){0,0,0,0};// default is 0 (not movable)
        if (y>=2 /*y>=2*/ && y+1<=mapHeight-2 && Map[y-1][x]!='@' && !hasBoxOrWall(boxesState, (Coordinate){x,(uint8_t)(y-1)}) && coloringMap[mapWidth*(y+1)+x]==colorNum_player){
            #pragma omp critical
            {
                it->second[DIR_W] = 1;
            }
        }
        if (y>=2 /*y-1>=1*/ && y+1<=mapHeight-2 && Map[y+1][x]!='@' && !hasBoxOrWall(boxesState, (Coordinate){x,(uint8_t)(y+1)}) && coloringMap[mapWidth*(y-1)+x]==colorNum_player){
            #pragma omp critical
            {
                it->second[DIR_S] = 1;
            }
        }
        if (x>=2 /*x-1>=1*/ && x+1<=mapWidth-2 && Map[y][x-1]!='@' && !hasBoxOrWall(boxesState, (Coordinate){(uint8_t)(x-1),y}) && coloringMap[mapWidth*y+(x+1)]==colorNum_player){
            #pragma omp critical
            {
                it->second[DIR_A] = 1;
            }
        }
        if (x>=2 /*x-1>=1*/ && x+1<=mapWidth-2 && Map[y][x+1]!='@' && !hasBoxOrWall(boxesState, (Coordinate){(uint8_t)(x+1),y}) && coloringMap[mapWidth*y+(x-1)]==colorNum_player){
            #pragma omp critical
            {
                it->second[DIR_D] = 1;
            }
        }
    }

    return boxesState;
}


bool hasBox(const BoxesState_Type& boxesState, const Coordinate queryCoord){
    return boxesState.find(queryCoord) != boxesState.end();
}

bool hasBoxOrWall(const BoxesState_Type& boxesState, const Coordinate queryCoord){
    uint8_t x = queryCoord.X;
    uint8_t y = queryCoord.Y;
    return (Map[y][x]=='#') || hasBox(boxesState, queryCoord);
}

bool isTarget(const Coordinate queryCoord){
    uint8_t x = queryCoord.X;
    uint8_t y = queryCoord.Y;
    return Map[y][x] == '.';
}

bool check_if_dead_state(const BoxesState_Type& boxesState, MoveTransition& moveTrans){

    uint8_t x = moveTrans.boxLoc.X;
    uint8_t y = moveTrans.boxLoc.Y;
    switch(moveTrans.dir){
        case DIR_W: y--; break;
        case DIR_S: y++; break;
        case DIR_A: x--; break;
        case DIR_D: x++; break;
    }

    // check if all boxes are immovable
    bool isAllImmovable = true;
    for (auto& boxState : boxesState){
        for (uint8_t d : boxState.second){
            if (d == 1){
                isAllImmovable = false;
                break;
            }
        }
        if (isAllImmovable == false) break;
    }
    if (isAllImmovable) return true;


    // check if enter a non-target corner or a deadlock
    if (x>=1 && y>=1  
        && hasBoxOrWall(boxesState,(Coordinate){(uint8_t)(x-1),(uint8_t)(y-1)}) // ##
        && hasBoxOrWall(boxesState,(Coordinate){x,(uint8_t)(y-1)})              // #x 
        && hasBoxOrWall(boxesState,(Coordinate){(uint8_t)(x-1),y})) {
        if (!isTarget((Coordinate){x,y})) return true; 
        if (Map[y-1][x-1]!='#' && !isTarget((Coordinate){(uint8_t)(x-1),(uint8_t)(y-1)})) return true; // is an immovable box not on a target
        if (Map[y-1][x]!='#' && !isTarget((Coordinate){x,(uint8_t)(y-1)})) return true;
        if (Map[y][x-1]!='#' && !isTarget((Coordinate){(uint8_t)(x-1),y})) return true;
    }
    if (x<=mapWidth-2 && y>=1  
        && hasBoxOrWall(boxesState,(Coordinate){(uint8_t)(x+1),(uint8_t)(y-1)}) // ##
        && hasBoxOrWall(boxesState,(Coordinate){x,(uint8_t)(y-1)})              // x# 
        && hasBoxOrWall(boxesState,(Coordinate){(uint8_t)(x+1),y})) {
        if (!isTarget((Coordinate){x,y})) return true;
        if (Map[y-1][x+1]!='#' && !isTarget((Coordinate){(uint8_t)(x+1),(uint8_t)(y-1)})) return true;
        if (Map[y-1][x]!='#' && !isTarget((Coordinate){x,(uint8_t)(y-1)})) return true;
        if (Map[y][x+1]!='#' && !isTarget((Coordinate){(uint8_t)(x+1),y})) return true;
    }
    if (x>=1 && y<=mapHeight-2  
        && hasBoxOrWall(boxesState,(Coordinate){(uint8_t)(x-1),(uint8_t)(y+1)}) // #x
        && hasBoxOrWall(boxesState,(Coordinate){x,(uint8_t)(y+1)})              // ## 
        && hasBoxOrWall(boxesState,(Coordinate){(uint8_t)(x-1),y})) {
        if (!isTarget((Coordinate){x,y})) return true;
        if (Map[y+1][x-1]!='#' && !isTarget((Coordinate){(uint8_t)(x-1),(uint8_t)(y+1)})) return true;
        if (Map[y+1][x]!='#' && !isTarget((Coordinate){x,(uint8_t)(y+1)})) return true;
        if (Map[y][x-1]!='#' && !isTarget((Coordinate){(uint8_t)(x-1),y})) return true;
    }
    if (x<=mapWidth-2 && y<=mapHeight-2  
        && hasBoxOrWall(boxesState,(Coordinate){(uint8_t)(x+1),(uint8_t)(y+1)}) // x#
        && hasBoxOrWall(boxesState,(Coordinate){x,(uint8_t)(y+1)})              // ## 
        && hasBoxOrWall(boxesState,(Coordinate){(uint8_t)(x+1),y})) {
        if (!isTarget((Coordinate){x,y})) return true;
        if (Map[y+1][x+1]!='#' && !isTarget((Coordinate){(uint8_t)(x+1),(uint8_t)(y+1)})) return true;
        if (Map[y+1][x]!='#' && !isTarget((Coordinate){x,(uint8_t)(y+1)})) return true;
        if (Map[y][x+1]!='#' && !isTarget((Coordinate){(uint8_t)(x+1),y})) return true;
    }



    // check # boxes at edge > # target 
    std::vector<QuotaElement> quotaVec_test(quotaVec.begin(), quotaVec.end()); // get a copy
    for (auto boxState : boxesState){
        int8_t x = boxState.first.X;
        int8_t y = boxState.first.Y;
        if (SpiderTrapMap[y][x] != -1){
            int8_t quotaElementIdx = SpiderTrapMap[y][x];
            if (quotaVec_test[quotaElementIdx].boxQuota <= 0) return true; // quota exceeded
            else quotaVec_test[quotaElementIdx].boxQuota--; 

            if (quotaVec_test[quotaElementIdx].chainIdx1 != -1) {
                int8_t chainIdx = quotaVec_test[quotaElementIdx].chainIdx1;
                if (quotaVec_test[chainIdx].boxQuota <= 0) return true; // quota exceeded
                else quotaVec_test[chainIdx].boxQuota--;
            }
            if (quotaVec_test[quotaElementIdx].chainIdx2 != -1) {
                int8_t chainIdx = quotaVec_test[quotaElementIdx].chainIdx2;
                if (quotaVec_test[chainIdx].boxQuota <= 0) return true; // quota exceeded
                else quotaVec_test[chainIdx].boxQuota--;
            }
        }
    }

    return false;
}


std::string dfs_find_move_seq(Coordinate from, Coordinate to, const BoxesState_Type& boxesState, bool* visitMap){
    uint8_t x = from.X;
    uint8_t y = from.Y;
    visitMap[mapWidth*y+x] = 1;

    if (from == to) return "";
    
    std::string subSeq;
    if (y >= 2 /*y-1 >= 1*/){
        if ((Coordinate){x,(uint8_t)(y-1)} == to) return "W";
        else if (!hasBoxOrWall(boxesState, (Coordinate){x,(uint8_t)(y-1)}) && visitMap[mapWidth*(y-1)+x]==0){
            subSeq = dfs_find_move_seq((Coordinate){.X=x, .Y=(uint8_t)(y-1)}, to, boxesState, visitMap);
            if (!subSeq.empty()) return "W" + subSeq;
        }
    }
    if (y+1 <= mapHeight-2){
        if ((Coordinate){x,(uint8_t)(y+1)} == to) return "S";
        else if (!hasBoxOrWall(boxesState, (Coordinate){x,(uint8_t)(y+1)}) && visitMap[mapWidth*(y+1)+x]==0){
            subSeq = dfs_find_move_seq((Coordinate){.X=x, .Y=(uint8_t)(y+1)}, to, boxesState, visitMap);
            if (!subSeq.empty()) return "S" + subSeq;
        }
    }
    if (x >= 2 /*x-1 >= 1*/){
        if ((Coordinate){(uint8_t)(x-1),y} == to) return "A";
        else if (!hasBoxOrWall(boxesState, (Coordinate){(uint8_t)(x-1),y}) && visitMap[mapWidth*y+(x-1)]==0){
            subSeq = dfs_find_move_seq((Coordinate){.X=(uint8_t)(x-1), .Y=y}, to, boxesState, visitMap);
            if (!subSeq.empty()) return "A" + subSeq;
        }

    }
    if (x+1 <= mapWidth-2){
        if ((Coordinate){(uint8_t)(x+1),y} == to) return "D";
        else if (!hasBoxOrWall(boxesState, (Coordinate){(uint8_t)(x+1),y}) && visitMap[mapWidth*y+(x+1)]==0){
            subSeq = dfs_find_move_seq((Coordinate){.X=(uint8_t)(x+1), .Y=y}, to, boxesState, visitMap);
            if (!subSeq.empty()) return "D" + subSeq;
        }
    }
    
    return "";
}


void output_ans(Coordinate lastPlayerLoc, State* parentState, MoveTransition lastMoveTrans){

    if (parentState != stateRoot) 
        output_ans(parentState->playerLoc, parentState->parentState, parentState->moveTrans);

    Coordinate from = parentState->playerLoc;
    uint8_t to_x_t = lastMoveTrans.boxLoc.X;
    uint8_t to_y_t = lastMoveTrans.boxLoc.Y;
    Coordinate to;
    char lastMoveKey;
    switch(lastMoveTrans.dir){
        case DIR_W: to = (Coordinate){.X = to_x_t, .Y = (uint8_t)(to_y_t+1)}; lastMoveKey='W'; break;
        case DIR_S: to = (Coordinate){.X = to_x_t, .Y = (uint8_t)(to_y_t-1)}; lastMoveKey='S'; break;
        case DIR_A: to = (Coordinate){.X = (uint8_t)(to_x_t+1), .Y = to_y_t}; lastMoveKey='A'; break;
        case DIR_D: to = (Coordinate){.X = (uint8_t)(to_x_t-1), .Y = to_y_t}; lastMoveKey='D'; break;
    }

    bool* visitMap;
    visitMap = (bool*)malloc(mapWidth*mapHeight*sizeof(bool));
    if (visitMap == NULL) exit(1);
    memset(visitMap, 0, mapWidth*mapHeight*sizeof(bool));

    std::string moveSeq = dfs_find_move_seq(from, to, parentState->boxesState, visitMap);
    free(visitMap);
    std::cout << moveSeq << lastMoveKey;
}


void set_spiderTrapMap(void){
    //TODO: consider '@'

    memset(SpiderTrapMap, -1, sizeof(SpiderTrapMap));
    
    // horizontal
    for (int8_t y=1; y <= mapHeight-2; y++){
        int8_t from=-1, to=-1;
        int8_t numTarget;
        for (int8_t x=1; x <= mapWidth-2; x++){
            if (Map[y][x]!='#' && (Map[y-1][x]=='#' || Map[y+1][x]=='#') && Map[y][x-1]=='#' && Map[y][x+1]!='#'){ // start point
                numTarget = (Map[y][x]=='.') ? 1 : 0;
                from = x;
            }
            else if (Map[y][x]!='#' && (Map[y-1][x]=='#' || Map[y+1][x]=='#') && Map[y][x+1]=='#' && from!=-1) { // end point
                if (Map[y][x] == '.') numTarget++;
                to = x;
                for (int8_t i=from; i <= to; i++) SpiderTrapMap[y][i] = newQuotaElementIdx;
                quotaVec.push_back((QuotaElement){.boxQuota = numTarget, .chainIdx1 = -1, .chainIdx2 = -1});
                numTarget = 0;
                newQuotaElementIdx++;
            }
            else if (Map[y][x]!='#' && (Map[y-1][x]=='#' || Map[y+1][x]=='#')){ // continue ...
                if (Map[y][x] == '.') numTarget++;
            } 
            else {
                from = -1;
                numTarget = 0;
            }
        } 
    }

    // vertical
    for (int8_t x=1; x <= mapWidth-2; x++){
        int8_t from=-1, to=-1;
        int8_t numTarget;
        for (int8_t y=1; y <= mapHeight-2; y++){
            if (Map[y][x]!='#' && (Map[y][x-1]=='#' || Map[y][x+1]=='#') && Map[y-1][x]=='#' && Map[y+1][x]!='#'){ // start point
                numTarget = (Map[y][x]=='.') ? 1 : 0;
                from = y;
            }
            else if (Map[y][x]!='#' && (Map[y][x-1]=='#' || Map[y][x+1]=='#') && Map[y+1][x]=='#' && from!=-1) { // end point
                if (Map[y][x] == '.') numTarget++;
                to = y;


                if (SpiderTrapMap[from][x]==-1 && SpiderTrapMap[to][x]==-1){
                    for (int8_t j=from; j <= to; j++) SpiderTrapMap[j][x] = newQuotaElementIdx;
                    quotaVec.push_back((QuotaElement){.boxQuota = numTarget, .chainIdx1 = -1, .chainIdx2 = -1});
                    numTarget = 0;
                    newQuotaElementIdx++;
                }
                // TODO


            }
            else if (Map[y][x]!='#' && (Map[y][x-1]=='#' || Map[y][x+1]=='#')){ // continue ...
                if (Map[y][x] == '.') numTarget++;
            } 
            else {
                from = -1;
                numTarget = 0;
            }
        } 
    }
}



